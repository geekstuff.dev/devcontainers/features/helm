# Geekstuff.dev / Devcontainers / Features / Helm

This devcontainer feature installs Helm, and its autocomplete.

## How to use

In your `.devcontainer/devcontainer.json`, add this feature elements:

```json
{
    "name": "my devcontainer",
    "image": "debian:bullseye",
    "features": {
        "ghcr.io/geekstuff-dev/devcontainers-features/basics": {},
        "ghcr.io/geekstuff-dev/devcontainers-features/kubectl": {},
        "ghcr.io/geekstuff-dev/devcontainers-features/helm": {}
    }
}
```

You can use a `debian`, `ubuntu` or `alpine` image as the base.

This feature does not require any other feature, but if you also use
the kubectl one, that one requires the basics feature.

Full list of source tags are [available here](https://gitlab.com/geekstuff.dev/devcontainers/features/helm/-/tags).
